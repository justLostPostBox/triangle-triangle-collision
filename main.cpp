#include <cmath>
#include <limits>
#include <iostream>

namespace RomanMi
{
    const double eps = std::numeric_limits<double>::epsilon();

    bool areEqual(double a, double b)
    {
        double diff = std::fabs(a - b);
        a = std::fabs(a);
        b = std::fabs(b);

        double min = ( a > b ) ? b : a;

        return ( diff <= min * eps );
    };

    class Point
    {
    public:

        double x_;
        double y_;
        double z_;

        Point() : x_(0.0), y_(0.0), z_(0.0)
        { };

        Point(double x, double y, double z) : x_(x), y_(y), z_(z)
        { };

        friend bool areEqual(const Point& lhs, const Point& rhs);
    };

    bool areEqual(const Point& lhs, const Point& rhs)
    {
        return areEqual(lhs.x_, rhs.x_)
               && areEqual(lhs.y_, rhs.y_)
               && areEqual(lhs.z_, rhs.z_);
    };

    class Vector
    {
    public:

        double x_;
        double y_;
        double z_;

        Vector() : x_(0.0), y_(0.0), z_(1.0)
        { };

        Vector(double x, double y, double z) : x_(x), y_(y), z_(z)
        { };

        Vector(const Point& start, const Point& end)
                : x_(end.x_ - start.x_), y_(end.y_ - start.y_), z_(end.z_ - start.z_)
        { };

        double length() const
        {
            return std::sqrt(x_ * x_ + y_ * y_ + z_ * z_);
        };

        double length2() const
        {
            return x_ * x_ + y_ * y_ + z_ * z_;
        };

    };

    Vector cross(const Vector& lhs, const Vector& rhs)
    {
        double a = lhs.y_ * rhs.z_ - lhs.z_ * rhs.y_;
        double b = lhs.z_ * rhs.x_ - lhs.x_ * rhs.z_;
        double c = lhs.x_ * rhs.y_ - lhs.y_ * rhs.x_;

        Vector result(a, b, c);
        return result;
    }

    double dot(const Vector& lhs, const Vector& rhs)
    {
        return lhs.x_ * rhs.x_ + lhs.y_ * rhs.y_ + lhs.z_ * rhs.z_;
    }

    class Segment
    {
    public:

        Point start_;
        Point end_;

        Segment() : start_(), end_(1.0, 1.0, 1.0)
        { }

        Segment(const Point& start, const Point& end) : start_(start), end_(end)
        { }

        bool checkPointOnSegment(const Point& point) const
        {
            Vector segVector(start_, end_);
            Vector pVector(start_, point);

            Vector vCrossProduct = cross(segVector, pVector);

            if (!areEqual(vCrossProduct.length2(), 0.0))
                return false; //point doesn't belongs the segment

            double dotProd = dot(segVector, pVector);
            if (dotProd < 0.0) return false;
            if (areEqual(dotProd, 0.0)) return true;

            double segLenth = dot(segVector, segVector);
            if (dotProd > segLenth) return false;
            if (areEqual(dotProd, segLenth)) return true;

            return true;
        }

        bool isIntersect(const Segment& other) const
        {
            Vector v1(start_, end_);
            Vector v2(other.start_, other.end_);

            Vector v13(start_, other.start_);
            Vector v14(start_, other.end_);

            Vector crossV12 = cross(v1, v2);

            if (!areEqual(dot(v13, crossV12), 0.0))
                return false; // are not complanar

            if(areEqual(crossV12.length2(), 0.0)
               && !this->checkPointOnSegment(other.start_) && !this->checkPointOnSegment(other.end_))
            {
                return false; // are parallel
            }

            double K1 = dot(v1, v1);
            double K2 = dot(v1, v13);
            double K3 = dot(v1, v14);

            if((areEqual(K2*K3, 0.) || areEqual(K3, K1))
               && (!this->checkPointOnSegment(other.start_) || !this->checkPointOnSegment(other.end_)))
            {
                return false;
            }

            if(K2 < 0. && K3 < 0.)
                return false;


            if((K3 && K2) > K1)
                return false;

            return true;
        }
    };

    class Triangle
    {
    public:

        Point v0_;
        Point v1_;
        Point v2_;

        Triangle(const Point& p0, const Point& p1, const Point& p2)
                : v0_(p0), v1_(p1), v2_(p2)
        { }

        bool checkPointInTriangle(const Point& point) const
        {
            Vector v01(v0_, v1_);
            Vector v02(v0_, v2_);

            Vector pv0(point, v0_);
            Vector pv1(point, v1_);
            Vector pv2(point, v2_);

            double doubledTriArea = cross(v01, v02).length();
            double fullArea = cross(pv0, pv1).length() + cross(pv0, pv2).length() + cross(pv1, pv2).length();

            return areEqual(doubledTriArea, fullArea);
        }

        bool isIntersect(const Segment& segment) const
        {
            Point origin;

            Vector e0(v0_, v1_);
            Vector e1(v0_, v2_);
            Vector norm = cross(e0, e1);

            double pd = dot(norm, Vector(origin, v0_));

            double signSrc = dot(norm, Vector(origin, segment.start_)) - pd;
            double signDst = dot(norm, Vector(origin, segment.end_)) - pd;

            if (signSrc * signDst > 0.0)
                return false;

            if (areEqual(signSrc * signDst, 0.0))
            {
                Segment edge0(v0_, v1_);
                Segment edge1(v0_, v2_);
                Segment edge2(v1_, v2_);

                return edge0.isIntersect(segment)
                       || edge1.isIntersect(segment)
                       || edge2.isIntersect(segment)
                       || this->checkPointInTriangle(segment.start_)
                       || this->checkPointInTriangle(segment.end_);
            };

            double d = signSrc / ( signSrc - signDst );

            //calculate coord. of point
            double pX = segment.start_.x_ + d * ( segment.end_.x_ - segment.start_.x_ );
            double pY = segment.start_.y_ + d * ( segment.end_.y_ - segment.start_.y_ );
            double pZ = segment.start_.z_ + d * ( segment.end_.z_ - segment.start_.z_ );

            Point pnt(pX, pY, pZ);
            return this->checkPointInTriangle(pnt);
        }

        bool isIntersect(const Triangle& triangle) const
        {
            if (this->isIntersect(Segment(triangle.v0_, triangle.v1_)))
                return true;
            if (this->isIntersect(Segment(triangle.v0_, triangle.v2_)))
                return true;
            if (this->isIntersect(Segment(triangle.v1_, triangle.v2_)))
                return true;

            if (triangle.isIntersect(Segment(v0_, v1_)))
                return true;
            if (triangle.isIntersect(Segment(v0_, v2_)))
                return true;
            if (triangle.isIntersect(Segment(v1_, v2_)))
                return true;

            return false;
        }
    };

    bool checkIntersection(const Point& lhs, const Point& rhs)
    {
        return areEqual(lhs, rhs);
    }

    bool checkIntersection(const Point& point, const Segment& segment)
    {
        return segment.checkPointOnSegment(point);
    }

    bool checkIntersection(const Segment& segment, const Point& point)
    {
        return checkIntersection(point, segment);
    }

    bool checkIntersection(const Point& point, const Triangle& triangle)
    {
        return triangle.checkPointInTriangle(point);
    }

    bool checkIntersection(const Triangle& triangle, const Point& point)
    {
        return checkIntersection(point, triangle);
    }

    bool checkIntersection(const Segment& lhs, const Segment& rhs)
    {
        return lhs.isIntersect(rhs);
    }

    bool checkIntersection(const Triangle& triangle, const Segment& segment)
    {
        return triangle.isIntersect(segment);
    }

    bool checkIntersection(const Segment& segment, const Triangle& triangle)
    {
        return checkIntersection(triangle, segment);
    }

    bool checkIntersection(const Triangle& triangle1, const Triangle& triangle2)
    {
        return triangle1.isIntersect(triangle2);
    }

    Segment createSegment(const Point& a, const Point& b, const Point& c)
    {
        return areEqual(a, b) ? Segment(a, c) : Segment(a, b);
    }


    int findConfiguration(const Point& a, const Point& b, const Point& c)
    {
        int label = 0;

        if (areEqual(a, b))
            label++;
        if (areEqual(a, c))
            label++;
        if (areEqual(b, c))
            label++;

        //0: triangle; 1: segment; 2: point
        return label;
    }

    bool checkIntersection(double t1[9], double t2[9])
    {
        Point v01(t1[0], t1[1], t1[2]);
        Point v11(t1[3], t1[4], t1[5]);
        Point v21(t1[6], t1[7], t1[8]);

        Point v02(t2[0], t2[1], t2[2]);
        Point v12(t2[3], t2[4], t2[5]);
        Point v22(t2[6], t2[7], t2[8]);

        int label1 = findConfiguration(v01, v11, v21);
        int label2 = findConfiguration(v02, v12, v22);

        if (label1 == 3 && label2 == 3) return checkIntersection(v01, v02);
        if (label1 == 3 && label2 == 1) return checkIntersection(v01, createSegment(v02, v12, v22));
        if (label1 == 3 && label2 == 0) return checkIntersection(v01, Triangle(v02, v12, v22));

        if (label1 == 1 && label2 == 0) return checkIntersection(createSegment(v01, v11, v21), Triangle(v02, v12, v22));
        if (label1 == 1 && label2 == 1) return checkIntersection(createSegment(v01, v11, v21), createSegment(v02, v12, v22));
        if (label1 == 1 && label2 == 3) return checkIntersection(createSegment(v01, v11, v21), v02);

        if (label1 == 0 && label2 == 3) return checkIntersection(Triangle(v01, v11, v21), v02);
        if (label1 == 0 && label2 == 1) return checkIntersection(Triangle(v01, v11, v21), createSegment(v02, v12, v22));
        if (label1 == 0 && label2 == 0) return checkIntersection(Triangle(v01, v11, v21), Triangle(v02, v12, v22));

        return false;
    }
}

int main ()
{
    using namespace RomanMi;

    return 0;
}